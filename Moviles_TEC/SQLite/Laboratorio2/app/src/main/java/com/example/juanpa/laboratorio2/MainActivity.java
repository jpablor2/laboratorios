package com.example.juanpa.laboratorio2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public Context getContext() {
        return context;
    }

    Context context;
    public void insertarInsumo(String codigoInsumo,String nombreInsumo,String cantidadInsumo,String unidadMedidaInsumo) {
        // Instancia la clase Estudiante y realiza la inserción de datos
        Insumo insumo = new Insumo(codigoInsumo,nombreInsumo,cantidadInsumo,unidadMedidaInsumo);
        // inserta el estudiante, se le pasa como parametro el contexto de la app
        //long newRowId = insumo.insertar(getApplicationContext());
        // Mostrar un mensaje para el usuario
        Toast.makeText(getApplicationContext(), "Insertar Insumo: " +//+ newRowId +
                " Id: " + insumo.getCodigoInsumo() +
                " Nombre: " + insumo.getNombreInsumo() + " Cantidad: " +
                insumo.getCantidadInsumo() +
                "Unidad:  " + insumo.getUnidadMedidaInsumo(), Toast.LENGTH_LONG).show();
    }

    public void leerEstudiante(String codigoInsumo) {
        // Instancia la clase Estudiante y realiza la lectura de datos
        Insumo insumo = new Insumo();
        // leer el estudiante, se le pasa como parametro el contexto de la app y ls identificacion
        insumo.leer(codigoInsumo);
        // si lee al estudiante
        try{
            // Mostrar un mensaje para el usuario
            Toast.makeText(getApplicationContext(), "Codigo insumo: " +
                    insumo.getCodigoInsumo() +
                    " Nombre Insumo: " + insumo.getNombreInsumo() + " Cantidad Insumo: " +
                    insumo.getCantidadInsumo() +
                    " Unidad " + insumo.getUnidadMedidaInsumo() , Toast.LENGTH_LONG).show();
        }
        catch (Exception e){
            Toast.makeText(getApplicationContext(), "Registro no encontrado", Toast.LENGTH_LONG).show();
        }
    }

    public void eliminarEstudiante(String codigoInsumo) {
        // Instancia la clase Estudiante y realiza el borrado de datos
        Insumo insumo = new Insumo();
        // leer el estudiante, se le pasa como parametro el contexto de la app y ls identificacion
        insumo.eliminar(codigoInsumo);
        // Mostrar un mensaje para el usuario
        Toast.makeText(getApplicationContext(), "Eliminar Insumo.", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context=getApplicationContext();

        setContentView(R.layout.activity_main);

        Button buttonInsertarInsumo = (Button)
                findViewById(R.id.btn_insertar_insumo);

        Button buttonInsertarOrden = (Button)
                findViewById(R.id.btn_insertar_orden);
        Button buttonInsertarPlato = (Button)
                findViewById(R.id.btn_insertar_plato);
        Button buttonMostrarOrdenes = (Button)
                findViewById(R.id.btn_mostrar_ordenes);

        buttonInsertarInsumo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //insertarEstudiante();
                llamarInsumo(v);

            }
        });
        buttonInsertarOrden.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //insertarEstudiante();
                llamarOrden(v);

            }
        });
        buttonInsertarPlato.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //insertarEstudiante();
                llamarPlato(v);

            }
        });
        buttonMostrarOrdenes.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //insertarEstudiante();
                llamarOrdenesHechas(v);

            }
        });

    }

    public void llamarInsumo(View view ){
        Intent intent = new Intent( view.getContext(), Insumo.class);
        startActivity(intent);
    }

    public void llamarOrden(View view ){
        Intent intent = new Intent( view.getContext(), Orden.class);
        startActivity(intent);
    }

    public void llamarPlato(View view ){
        Intent intent = new Intent( view.getContext(), Plato.class);
        startActivity(intent);
    }
    public void llamarOrdenesHechas(View view ){
        Intent intent = new Intent( view.getContext(), OrdenesHechas.class);
        startActivity(intent);
    }
}
