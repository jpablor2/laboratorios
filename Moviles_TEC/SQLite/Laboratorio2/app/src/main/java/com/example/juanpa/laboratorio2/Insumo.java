package com.example.juanpa.laboratorio2;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.Date;

import static com.example.juanpa.laboratorio2.R.id.linear_lista;

/**
 * Created by Juan Pablo on 17/3/2017.
 */

public class Insumo extends AppCompatActivity {

    private String codigoInsumo;
    private String nombreInsumo;
    private String cantidadInsumo;
    private String unidadMedidaInsumo;
    private String idLeerBorrar;

    EditText txt_codigoInsumo ;
    EditText txt_cantidadInsumo ;
    EditText txt_nombreInsumo;
    EditText txt_unidadMedidaInsumo;
    EditText txt_borrar_leer_insumo;
    Button buttonInsertarInsumo;
    Button buttonBorrarInsumo;
    Button buttonLeerInsumo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insumo);

        txt_nombreInsumo = (EditText) findViewById(R.id.txt_nombre_insumo) ;
        txt_cantidadInsumo = (EditText) findViewById(R.id.txt_cantidad_insumo) ;
        txt_unidadMedidaInsumo = (EditText) findViewById(R.id.txt_unidad_medida_insumo) ;
        txt_codigoInsumo = (EditText) findViewById(R.id.txt_codigo_insumo) ;
        txt_borrar_leer_insumo=(EditText)findViewById(R.id.txt_borrar_leer_insumo);
        buttonInsertarInsumo = (Button) findViewById(R.id.btn_insertar_insumo);
        buttonLeerInsumo = (Button) findViewById(R.id.btn_leer_insumo);
        buttonBorrarInsumo = (Button) findViewById(R.id.btn_borrar_insumo);

        //buttonInsertarInsumo.setOnClickListener(this);

        buttonBorrarInsumo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String idInsumo = txt_borrar_leer_insumo.getText().toString();
                setIdLeerBorrar(idInsumo);
                //Toast.makeText(getApplicationContext(), idInsumo,Toast.LENGTH_LONG).show();

                eliminar(idInsumo);
                leer(idInsumo);

            }
        });

        buttonLeerInsumo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String idInsumo = txt_borrar_leer_insumo.getText().toString();
                setIdLeerBorrar(idInsumo);
                //Toast.makeText(getApplicationContext(), idInsumo,Toast.LENGTH_LONG).show();
                leer(idInsumo);

            }
        });

        buttonInsertarInsumo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                String codigoInsumo=txt_codigoInsumo.getText().toString();
                setCodigoInsumo(codigoInsumo);
                String nombreInsumo=txt_nombreInsumo.getText().toString();
                setNombreInsumo(nombreInsumo);
                String cantidadInsumo=txt_cantidadInsumo.getText().toString();
                setCantidadInsumo(cantidadInsumo);
                String unidadMedidaInsumo=txt_unidadMedidaInsumo.getText().toString();
                setUnidadMedidaInsumo(unidadMedidaInsumo);

                //
                Insumo insumo = new Insumo(codigoInsumo,nombreInsumo,cantidadInsumo,unidadMedidaInsumo);

                insertar();


            }
        });
    }

    public Insumo() {
    }

    public Insumo(String codigoInsumo, String nombreInsumo, String cantidadInsumo, String unidadMedidaInsumo) {
        this.codigoInsumo = codigoInsumo;
        this.nombreInsumo = nombreInsumo;
        this.cantidadInsumo = cantidadInsumo;
        this.unidadMedidaInsumo = unidadMedidaInsumo;
    }

    public void setIdLeerBorrar(String idLeerBorrar) {
        this.idLeerBorrar = idLeerBorrar;
    }

    public String getIdLeerBorrar() {
        return idLeerBorrar;
    }

    public String getCodigoInsumo() {
        return codigoInsumo;
    }

    public void setCodigoInsumo(String codigoInsumo) {
        this.codigoInsumo = codigoInsumo;
    }

    public String getNombreInsumo() {
        return nombreInsumo;
    }

    public void setNombreInsumo(String nombreInsumo) {
        this.nombreInsumo = nombreInsumo;
    }

    public String getCantidadInsumo() {
        return cantidadInsumo;
    }

    public void setCantidadInsumo(String cantidadInsumo) {
        this.cantidadInsumo = cantidadInsumo;
    }

    public String getUnidadMedidaInsumo() {
        return unidadMedidaInsumo;
    }

    public void setUnidadMedidaInsumo(String unidadMedidaInsumo) {
        this.unidadMedidaInsumo = unidadMedidaInsumo;
    }


    public void limpiar() {
        setNombreInsumo("");
        setCodigoInsumo("");
        setUnidadMedidaInsumo("");
        setCantidadInsumo("");
        txt_codigoInsumo.setText("");
        txt_nombreInsumo.setText("");
        txt_cantidadInsumo.setText("");
        txt_unidadMedidaInsumo.setText("");
        txt_borrar_leer_insumo.setText("");

    }

    // insertar un estudiante en la base de datos
    private void insertar() {
        // inserta la persona antes del estudiante
        //long newRowId = insertar(context);
        // si inserta la Persona inserto el estudiante

        Toast.makeText(getApplicationContext(), "Insertar Insumo: " +//+ newRowId +
                " Id: " + getCodigoInsumo() +
                " Nombre: " + getNombreInsumo() + " Cantidad: " +
                getCantidadInsumo() +
                "Unidad:  " + getUnidadMedidaInsumo(), Toast.LENGTH_LONG).show();

            // usar la clase DataBaseHelper para realizar la operacion de insertar
            DataBaseHelper dataBaseHelper = new DataBaseHelper(getApplicationContext());
            // Obtiene la base de datos en modo escritura
            SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
            // Crear un mapa de valores donde las columnas son las llaves
            ContentValues values = new ContentValues();
            values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_CODIGO_INSUMO_CINSUMO, getCodigoInsumo());
            values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_NOMBRE_INSUMO, getNombreInsumo());
            values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_CANTIDAD_INSUMO, getCantidadInsumo());
            values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_UNIDAD_MEDIDA_INSUMO, getUnidadMedidaInsumo());
            // Insertar la nueva fila
            db.insert(DataBaseContract.DataBaseEntry.TABLE_NAME_INSUMO, null, values);
            limpiar();

        //return newRowId;
    }

    // leer un estudiante desde la base de datos
    public void leer (String identificacion){
        // leer la persona antes del estudiante

        // si lee a la persona, leo el estudiante
            // usar la clase DataBaseHelper para realizar la operacion de select
            DataBaseHelper dataBaseHelper = new DataBaseHelper(getApplicationContext());
            // Obtiene la base de datos en modo lectura
            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            // Define cuales columnas quiere solicitar // en este caso todas las de la clase
            String[] projection = {DataBaseContract.DataBaseEntry.COLUMN_NAME_CODIGO_INSUMO_CINSUMO, DataBaseContract.DataBaseEntry.COLUMN_NAME_NOMBRE_INSUMO,
                    DataBaseContract.DataBaseEntry.COLUMN_NAME_CANTIDAD_INSUMO, DataBaseContract.DataBaseEntry.COLUMN_NAME_UNIDAD_MEDIDA_INSUMO
            };
            // Filtro para el WHERE
            String selection = DataBaseContract.DataBaseEntry.COLUMN_NAME_CODIGO_INSUMO_CINSUMO + " = ?";
            String[] selectionArgs = {identificacion};
            // Resultados en el cursor
            Cursor cursor = db.query(
                    DataBaseContract.DataBaseEntry.TABLE_NAME_INSUMO, // tabla
                    projection, // columnas
                    selection, // where
                    selectionArgs, // valores del where
                    null, //agrupamiento
                    null, // filtros por grupo
                    null // orden
            );

            // recorrer los resultados y asignarlos a la clase // aca podria implementarse un ciclo si es necesario

            if (cursor.moveToFirst() && cursor.getCount() > 0) {
                setCodigoInsumo(cursor.getString(cursor.getColumnIndexOrThrow(
                        DataBaseContract.DataBaseEntry.COLUMN_NAME_CODIGO_INSUMO_CINSUMO)));
                setNombreInsumo(cursor.getString(cursor.getColumnIndexOrThrow(
                        DataBaseContract.DataBaseEntry.COLUMN_NAME_NOMBRE_INSUMO)));
                setCantidadInsumo(cursor.getString(cursor.getColumnIndexOrThrow(
                        DataBaseContract.DataBaseEntry.COLUMN_NAME_CANTIDAD_INSUMO)));
                setUnidadMedidaInsumo(cursor.getString(cursor.getColumnIndexOrThrow(
                        DataBaseContract.DataBaseEntry.COLUMN_NAME_UNIDAD_MEDIDA_INSUMO)));
            }

        try{
            // Mostrar un mensaje para el usuario
            Toast.makeText(getApplicationContext(), "Codigo insumo: " +
                    getCodigoInsumo() +
                    " Nombre Insumo: " + getNombreInsumo() + " Cantidad Insumo: " +
                    getCantidadInsumo() +
                    " Unidad " + getUnidadMedidaInsumo() , Toast.LENGTH_LONG).show();
        }
        catch (Exception e){
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
        limpiar();
    }

    // eliminar un estudiante desde la base de datos
    public void eliminar (String identificacion) {
        // usar la clase DataBaseHelper para realizar la operacion de eliminar
        DataBaseHelper dataBaseHelper = new DataBaseHelper(getApplicationContext());
        // Obtiene la base de datos en modo escritura
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        // Define el where para el borrado
        String selection = DataBaseContract.DataBaseEntry.COLUMN_NAME_CODIGO_INSUMO_CINSUMO + " LIKE ?";
        // Se detallan los argumentos
        String[] selectionArgs = {identificacion};
        // Realiza el SQL de borrado
        db.delete(DataBaseContract.DataBaseEntry.TABLE_NAME_INSUMO, selection, selectionArgs);
        // eliminar la persona despues del estudiante
        Toast.makeText(getApplicationContext(), "Eliminando "+identificacion,Toast.LENGTH_LONG).show();
        limpiar();
    }

    // actualizar un estudiante en la base de datos

}
