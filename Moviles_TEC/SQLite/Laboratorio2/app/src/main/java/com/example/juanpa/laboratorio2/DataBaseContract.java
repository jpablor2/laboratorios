package com.example.juanpa.laboratorio2;

import android.provider.BaseColumns;

/**
 * Created by JuanPa on 28/04/2017.
 */

public class DataBaseContract {
    // Implementa la interfaz BaseColumns para heredar campos estandar del marco de Android _ID

    public static class DataBaseEntry implements BaseColumns {

        // Clase

        public static final String TABLE_NAME_ORDEN = "Orden";

        // private String identificacion; Utilizamos DataBaseEntry._ID de BaseColumns

        public static final String COLUMN_NAME_CODIGO_ORDEN_CORDEN = "codigo_orden";

        public static final String COLUMN_NAME_CODIGO_PLATO_CORDEN = "codigo_plato";

        public static final String COLUMN_NAME_FECHA = "fecha";

        public static final String COLUMN_NAME_HORA = "hora";

        public static final String COLUMN_NAME_COMENTARIO = "comentario";




        // Clase Estudiante

        public static final String TABLE_NAME_PLATO = "Plato";

        // private String carnet;

        public static final String COLUMN_NAME_CODIGO_PLATO_CPLATO = "codigo_plato";

        // private int carreraBase;

        public static final String COLUMN_NAME_NOMBRE_PLATO = "nombre_plato";

        // private double promedioPonderado;

        public static final String COLUMN_NAME_DESCRIPCION = "descripcion";

        public static final String COLUMN_NAME_PRECIO = "precio";


        // Clase Funcionario

        public static final String TABLE_NAME_INSUMO = "Insumo";

        public static final String COLUMN_NAME_CODIGO_INSUMO_CINSUMO = "codigo_insumo";


        public static final String COLUMN_NAME_NOMBRE_INSUMO = "nombre_insumo";

        public static final String COLUMN_NAME_CANTIDAD_INSUMO = "cantidad_insumo";

        public static final String COLUMN_NAME_UNIDAD_MEDIDA_INSUMO = "unidad_medida_insumo";

        // Clase Funcionario

        public static final String TABLE_NAME_INSUMO_PLATO = "Insumo_Plato";

        // private int unidadBase;

        public static final String COLUMN_NAME_CODIGO_INSUMO_CINSUMO_PLATO = "codigo_insumo";

        // private int puestoBase;

        public static final String COLUMN_NAME_CODIGO_PLATO_CINSUMO_PLATO = "codigo_plato";

    }

// Construir las tablas de la base de datos

    private static final String TEXT_TYPE = " TEXT";

    private static final String INTEGER_TYPE = " INTEGER";

    private static final String REAL_TYPE = " REAL";

    private static final String COMMA_SEP = ",";


    // Creacion de tablas Persona, Estudiante, Funcionario

    public static final String SQL_CREATE_ORDEN =

            "CREATE TABLE " + DataBaseEntry.TABLE_NAME_ORDEN + " (" +

                    DataBaseEntry.COLUMN_NAME_CODIGO_ORDEN_CORDEN + TEXT_TYPE + " PRIMARY KEY," +

                    DataBaseEntry.COLUMN_NAME_CODIGO_PLATO_CORDEN + TEXT_TYPE + COMMA_SEP +

                    DataBaseEntry.COLUMN_NAME_FECHA + TEXT_TYPE + COMMA_SEP +

                    DataBaseEntry.COLUMN_NAME_HORA + TEXT_TYPE + COMMA_SEP +

                    DataBaseEntry.COLUMN_NAME_COMENTARIO + TEXT_TYPE + COMMA_SEP+

                    "FOREIGN KEY(" + DataBaseEntry.COLUMN_NAME_CODIGO_PLATO_CORDEN +

                    ") REFERENCES " +

                    DataBaseEntry.TABLE_NAME_PLATO + "(" + DataBaseEntry.COLUMN_NAME_CODIGO_PLATO_CPLATO + " ))";


    public static final String SQL_DELETE_ORDEN =

            "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_ORDEN;


    public static final String SQL_CREATE_PLATO =

            "CREATE TABLE " + DataBaseEntry.TABLE_NAME_PLATO + " (" +

                    DataBaseEntry.COLUMN_NAME_CODIGO_PLATO_CPLATO + TEXT_TYPE + "PRIMARY KEY," +

                    DataBaseEntry.COLUMN_NAME_NOMBRE_PLATO + TEXT_TYPE + COMMA_SEP +

                    DataBaseEntry.COLUMN_NAME_DESCRIPCION + TEXT_TYPE + COMMA_SEP +

                    DataBaseEntry.COLUMN_NAME_PRECIO + TEXT_TYPE + ")";

    public static final String SQL_DELETE_PLATO =

            "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_PLATO;

    public static final String SQL_CREATE_INSUMO_PLATO =
            "CREATE TABLE " + DataBaseEntry.TABLE_NAME_INSUMO_PLATO + " (" +

                    DataBaseEntry.COLUMN_NAME_CODIGO_PLATO_CINSUMO_PLATO + TEXT_TYPE + COMMA_SEP +

                    DataBaseEntry.COLUMN_NAME_CODIGO_INSUMO_CINSUMO_PLATO + TEXT_TYPE + COMMA_SEP +

                    "PRIMARY KEY (" + DataBaseEntry.COLUMN_NAME_CODIGO_INSUMO_CINSUMO_PLATO + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_CODIGO_INSUMO_CINSUMO_PLATO+ "),"+

                    "FOREIGN KEY(" + DataBaseEntry.COLUMN_NAME_CODIGO_PLATO_CINSUMO_PLATO +") REFERENCES " +

                DataBaseEntry.TABLE_NAME_PLATO + "(" + DataBaseEntry.COLUMN_NAME_CODIGO_PLATO_CPLATO + ")"
                    + COMMA_SEP + "FOREIGN KEY(" + DataBaseEntry.COLUMN_NAME_CODIGO_INSUMO_CINSUMO +") REFERENCES " +

                     DataBaseEntry.TABLE_NAME_INSUMO + "(" + DataBaseEntry._ID + "))";

            /*

            "CREATE TABLE " + DataBaseEntry.TABLE_NAME_INSUMO_PLATO + " (" +

                    DataBaseEntry.COLUMN_NAME_CODIGO_PLATO_CINSUMO_PLATO + TEXT_TYPE + " PRIMARY KEY, " +

                    DataBaseEntry.COLUMN_NAME_CODIGO_INSUMO_CINSUMO_PLATO + TEXT_TYPE + " PRIMARY KEY, " +

                    " FOREING KEY(" + DataBaseEntry.COLUMN_NAME_CODIGO_PLATO_CINSUMO_PLATO +

                    ") REFERENCES " +

                    DataBaseEntry.TABLE_NAME_PLATO + "(" + DataBaseEntry.COLUMN_NAME_CODIGO_PLATO_CPLATO +


                    ")"+COMMA_SEP+" FOREIGN KEY(" + DataBaseEntry.COLUMN_NAME_CODIGO_INSUMO_CINSUMO_PLATO +

                    ") REFERENCES " +

                    DataBaseEntry.TABLE_NAME_INSUMO + "(" + DataBaseEntry.COLUMN_NAME_CODIGO_INSUMO_CINSUMO +

                    "))";*/


    public static final String SQL_DELETE_FUNCIONARIO =

            "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_INSUMO_PLATO;

    public static final String SQL_CREATE_INSUMO =

            "CREATE TABLE " + DataBaseEntry.TABLE_NAME_INSUMO + "(" +

                    DataBaseEntry.COLUMN_NAME_CODIGO_INSUMO_CINSUMO + TEXT_TYPE + "PRIMARY KEY," +

                    DataBaseEntry.COLUMN_NAME_NOMBRE_INSUMO + TEXT_TYPE + COMMA_SEP +

                    DataBaseEntry.COLUMN_NAME_CANTIDAD_INSUMO + TEXT_TYPE + COMMA_SEP +

                    DataBaseEntry.COLUMN_NAME_UNIDAD_MEDIDA_INSUMO + TEXT_TYPE + ")";

    public static final String SQL_DELETE_INSUMO =

            "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_INSUMO;
}

