package com.example.gabriel.mydatabsetec;

import android.graphics.Region;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MyDatabseTec extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_databse_tec);

        // Instanciar los botones del layout activity_main.xml
        Button buttonDataBaseInsert = (Button)
                findViewById(R.id.buttonDataBaseInsert);
        Button buttonDataBaseSelect = (Button)
                findViewById(R.id.buttonDataBaseSelect);
        Button buttonDataBaseUpdate = (Button)
                findViewById(R.id.buttonDataBaseUpdate);
        Button buttonDataBaseDelete = (Button)
                findViewById(R.id.buttonDataBaseDelete);

        buttonDataBaseInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertarFuncionario();
            }
        });

        buttonDataBaseSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leerFuncionario();
            }
        });

        buttonDataBaseUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actualizarFuncionario();
            }
        });

        buttonDataBaseDelete.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eliminarFuncionario();
            }
        }));
    }

    private void insertarEstudiante() {
        // Instancia la clase Estudiante y realiza la inserción de datos
        Estudiante estudiante = new Estudiante("1-1000-1000",
                "estudiante01@ucr.ac.cr", "Juan",
                "Perez", "Soto", "2511-0000", "8890-0000",
                UtilDates.StringToDate("01 01 1995"),
                Persona.TIPO_ESTUDIANTE, Persona.GENERO_MASCULINO, "A99148", 1, 8.0);
        // inserta el estudiante, se le pasa como parametro el contexto de la app
        long newRowId = estudiante.insertar(getApplicationContext());
        // Mostrar un mensaje para el usuario
        Toast.makeText(getApplicationContext(), "Insertar Estudiante: " + newRowId +
                " Id: " + estudiante.getIdentificacion() +
                " Carnet: " + estudiante.getCarnet() + " Nombre: " +
                estudiante.getNombre() +
                " " + estudiante.getPrimerApellido() + " " +
                estudiante.getSegundoApellido() +
                " Correo: " + estudiante.getCorreo() + " Tipo: " +
                estudiante.getTipo() + " Promedio: " +
                estudiante.getPromedioPonderado(), Toast.LENGTH_LONG).show();
    }

    private void leerEstudiante() {
        // Instancia la clase Estudiante y realiza la lectura de datos
        Estudiante estudiante = new Estudiante();
        // leer el estudiante, se le pasa como parametro el contexto de la app y ls identificacion
        estudiante.leer(getApplicationContext(), "1-1000-1000");
        // si lee al estudiante
        if (estudiante.getTipo().equals(Persona.TIPO_ESTUDIANTE)) {
            // Mostrar un mensaje para el usuario
            Toast.makeText(getApplicationContext(), "Leer Estudiante: " +
                    estudiante.getIdentificacion() +
                    " Carnet: " + estudiante.getCarnet() + " Nombre: " +
                    estudiante.getNombre() +
                    " " + estudiante.getPrimerApellido() + " " +
                    estudiante.getSegundoApellido() +
                    " Correo: " + estudiante.getCorreo() + " Tipo: " +
                    estudiante.getTipo() + " Promedio: " +
                    estudiante.getPromedioPonderado(), Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(getApplicationContext(), "No hay datos para: " + "1-1000-1000", Toast.LENGTH_LONG).show();
        }
    }

    private void eliminarEstudiante() {
        // Instancia la clase Estudiante y realiza el borrado de datos
        Estudiante estudiante = new Estudiante();
        // leer el estudiante, se le pasa como parametro el contexto de la app y ls identificacion
        estudiante.eliminar(getApplicationContext(), "1-1000-1000");
        // Mostrar un mensaje para el usuario
        Toast.makeText(getApplicationContext(), "Eliminar Estudiante.", Toast.LENGTH_LONG).show();
    }

    private void actualizarEstudiante() {
        // Instancia la clase Estudiante y realiza la actualización de datos
        Estudiante estudiante = new Estudiante("1-1000-1000",
                "estudiante01@ucr.ac.cr*", "Juan*",
                "Perez*", "Soto*", "2511-0000*", "8890-0000*",
                UtilDates.StringToDate("01 01 1995"),
                Persona.TIPO_ESTUDIANTE, Persona.GENERO_MASCULINO, "A99148*", 1, 8.0);
        // actualiza el estudiante, se le pasa como parametro el contexto de la app
        int contador = estudiante.actualizar(getApplicationContext());
        // si actualiza al estudiante
        if (contador >0) {
            // Mostrar un mensaje para el usuario
            Toast.makeText(getApplicationContext(), "Actualizar Estudiante: " +
                    contador + " Id: " + estudiante.getIdentificacion() +
                    " Carnet: " + estudiante.getCarnet() + " Nombre: " +
                    estudiante.getNombre() +
                    " " + estudiante.getPrimerApellido() + " " +
                    estudiante.getSegundoApellido() +
                    " Correo: " + estudiante.getCorreo() + " Tipo: " +
                    estudiante.getTipo() + " Promedio: " +
                    estudiante.getPromedioPonderado(), Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(getApplicationContext(), "No hay datos para: " +
                    estudiante.getIdentificacion(), Toast.LENGTH_LONG).show();
        }
    }



    //Metodos funcionario
    private void insertarFuncionario() {
        // Instancia la clase Funcionario y realiza la inserción de datos
        Funcionario funcionario = new Funcionario("1-1000-1000",
                "estudiante01@ucr.ac.cr", "Juan",
                "Perez", "Soto", "2511-0000", "8890-0000",
                UtilDates.StringToDate("01 01 1995"),
                Persona.TIPO_ADMINISTRATIVO, Persona.GENERO_MASCULINO, 100, 1, 8.0);
        // inserta el funcionario, se le pasa como parametro el contexto de la app
        long newRowId = funcionario.insertar(getApplicationContext());
        // Mostrar un mensaje para el usuario
        Toast.makeText(getApplicationContext(), "Insertar Funcionario: " + newRowId +
                " Id: " + funcionario.getIdentificacion() +
                " Unidad base: " + funcionario.getUnidadBase() + " Nombre: " +
                funcionario.getNombre() +
                " " + funcionario.getPrimerApellido() + " " +
                funcionario.getSegundoApellido() +
                " Correo: " + funcionario.getCorreo() + " Tipo: " +
                funcionario.getTipo() + " Puesto base: " +
                funcionario.getPuestoBase() + "Salario base: " +
                funcionario.getSalarioBase(), Toast.LENGTH_LONG).show();
    }

    private void leerFuncionario() {
        // Instancia la clase Funcionario y realiza la lectura de datos
        Funcionario funcionario = new Funcionario();
        // leer el funcionario, se le pasa como parametro el contexto de la app y ls identificacion
        funcionario.leer(getApplicationContext(), "1-1000-1000");
        // si lee al funcionario
        if (funcionario.getTipo().equals(Persona.TIPO_ADMINISTRATIVO)) {
            // Mostrar un mensaje para el usuario
            Toast.makeText(getApplicationContext(), "Leer Funcionario: " +
                    funcionario.getIdentificacion() +
                    " Unidad base: " + funcionario.getUnidadBase() + " Nombre: " +
                    funcionario.getNombre() +
                    " " + funcionario.getPrimerApellido() + " " +
                    funcionario.getSegundoApellido() +
                    " Correo: " + funcionario.getCorreo() + " Tipo: " +
                    funcionario.getTipo() + " Puesto base: " +
                    funcionario.getPuestoBase() + " Salario base: " +
                    funcionario.getSalarioBase(), Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(getApplicationContext(), "No hay datos para: " + "1-1000-1000", Toast.LENGTH_LONG).show();
        }
    }

    private void eliminarFuncionario() {
        // Instancia la clase Funcionario y realiza el borrado de datos
        Funcionario funcionario = new Funcionario();
        // leer el funcionario, se le pasa como parametro el contexto de la app y ls identificacion
        funcionario.eliminar(getApplicationContext(), "1-1000-1000");
        // Mostrar un mensaje para el usuario
        Toast.makeText(getApplicationContext(), "Eliminar Funcionario.", Toast.LENGTH_LONG).show();
    }

    private void actualizarFuncionario() {
        // Instancia la clase Funcionario y realiza la actualización de datos
        Funcionario funcionario = new Funcionario("1-1000-1000",
                "estudiante01@ucr.ac.cr*", "Juan*",
                "Perez*", "Soto*", "2511-0000*", "8890-0000*",
                UtilDates.StringToDate("01 01 1995"),
                Persona.TIPO_ADMINISTRATIVO, Persona.GENERO_MASCULINO, 200, 2, 9.0);
        // actualiza el funcionario, se le pasa como parametro el contexto de la app
        int contador = funcionario.actualizar(getApplicationContext());
        // si actualiza al funcionario
        if (contador >0) {
            // Mostrar un mensaje para el usuario
            Toast.makeText(getApplicationContext(), "Actualizar Funcionario: " +
                    contador + " Id: " + funcionario.getIdentificacion() +
                    " Unidad base: " + funcionario.getUnidadBase() + " Nombre: " +
                    funcionario.getNombre() +
                    " " + funcionario.getPrimerApellido() + " " +
                    funcionario.getSegundoApellido() +
                    " Correo: " + funcionario.getCorreo() + " Tipo: " +
                    funcionario.getTipo() + " Puesto base: " +
                    funcionario.getPuestoBase() + " Salario base: " +
                    funcionario.getSalarioBase(), Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(getApplicationContext(), "No hay datos para: " +
                    funcionario.getIdentificacion(), Toast.LENGTH_LONG).show();
        }
    }
}
