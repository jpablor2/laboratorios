package com.example.juanpa.labmapas;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.Locale;

public class MapsActivity extends AppCompatActivity implements
        OnMapReadyCallback , GoogleMap.OnMarkerClickListener ,
        GoogleMap.OnMarkerDragListener , GoogleMap.OnInfoWindowClickListener
        {
            private static final int LOCATION_REQUEST_CODE = 101;
            private GoogleMap mMap;

            private Marker markerTEC;
            private Marker markerCQ;
            private Marker markerTOSCANA;
            private static final LatLng TEC = new LatLng(10.3652,-84.512);
            private static final LatLng CQ = new LatLng(10.3271, -84.4357);
            private static final LatLng TOSCANA = new LatLng(10.3621,-84.5144);


            private Marker markerPais;
            public static final String EXTRA_LATITUDE =" latitude ";
            public static final String EXTRA_LONGITUDE =" longitude ";

            @Override
            public boolean onCreateOptionsMenu(Menu menu) {
                getMenuInflater().inflate(R.menu.menu,menu);
                return true;

                //return super.onCreateOptionsMenu(menu);
            }
            @Override
            public boolean onOptionsItemSelected(MenuItem item) {
                switch (item.getItemId())
                {
                    case R.id.m_hibrido:
                        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID); // Map type
                        break;
                    case R.id.m_normal:
                        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL); // Map type
                        break;
                    case R.id.m_Satelite:
                        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE); // Map type
                        break;
                    case R.id.m_terreno :
                        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN); // Map type
                        break;

                }
                return false;
            }

            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super .onCreate(savedInstanceState);
                setContentView(R.layout.activity_maps);

                SupportMapFragment mapFragment = (SupportMapFragment)
                        getSupportFragmentManager()
                                .findFragmentById(R.id.myMap);
                mapFragment.getMapAsync( this ) ;
            }
            protected void requestPermission(String permissionType ,
                                             int requestCode) {
                ActivityCompat.requestPermissions(this ,
                        new String[]{permissionType}, requestCode
                ) ;
            }
            @Override
            public void onRequestPermissionsResult( int requestCode ,
                                                    String permissions[], int []
                                                            grantResults) {
                switch (requestCode) {
                    case LOCATION_REQUEST_CODE: {
                        if (grantResults.length == 0
                                || grantResults [0] !=
                                PackageManager.PERMISSION_GRANTED) {
                            Toast.makeText(this ,
                                    " Unable to show location - permission required ",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            SupportMapFragment mapFragment =
                                    (SupportMapFragment) getSupportFragmentManager()
                                            .findFragmentById(R.id.myMap);
                            mapFragment.getMapAsync( this ) ;
                        }
                    }
                }
            }


            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                if (mMap != null ) {
                    int permission = ContextCompat.checkSelfPermission(this ,
                            Manifest.permission.ACCESS_FINE_LOCATION);
                    if (permission == PackageManager.PERMISSION_GRANTED) {
                        mMap.setMyLocationEnabled( true ) ;
                    } else {
                        requestPermission(
                                Manifest.permission.ACCESS_FINE_LOCATION ,
                                LOCATION_REQUEST_CODE);
                    }

                }

                UiSettings mapSettings;
                mapSettings = mMap.getUiSettings();

                markerTEC=mMap.addMarker ( new MarkerOptions()
                . position ( TEC )
                . title ("TEC Santa Clara"));
                CameraPosition cameraPosition
                = new CameraPosition. Builder ()
                . target ( TEC )
                . zoom (50)
                . bearing (70)
                . tilt (25)
                . build ();

                markerCQ=mMap.addMarker ( new MarkerOptions()
                        . position ( CQ )
                        . title ("Ciudad Quesada")
                        .draggable(true));

                markerTOSCANA=mMap.addMarker ( new MarkerOptions()
                        . position ( TOSCANA)
                        . title ("Cafetería Toscana")
                        .snippet("Excelente café y deliciosos arrollados de canela")
                        .icon(BitmapDescriptorFactory.fromResource (
                                R. drawable . cafe )));


                googleMap.setOnMarkerDragListener( this ) ;
                googleMap.setOnMarkerClickListener( this ) ;
                googleMap.setOnInfoWindowClickListener( this ) ;
                mMap . animateCamera ( CameraUpdateFactory . newCameraPosition ( cameraPosition ));
                mapSettings.setCompassEnabled(true); // Set Compass
                googleMap . moveCamera ( CameraUpdateFactory. newLatLng ( TEC ));

                Polyline line = mMap . addPolyline ( new PolylineOptions()
                        . add (TEC , CQ)
                        . width (25)
                        . color ( Color. BLUE )
                        . geodesic ( false ));



                mapSettings.setZoomControlsEnabled( true ) ; // Set Zoom Controls
                mapSettings.setAllGesturesEnabled( true ); // Set All Gestures
                mapSettings.setScrollGesturesEnabled( true ) ; // Set Scroll
                mapSettings.setTiltGesturesEnabled( true ) ; // Set Tilt
                mapSettings.setRotateGesturesEnabled( true ) ; // Set Rotate
                mMap.setBuildingsEnabled( true ) ; //3D view
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE); // Map type
            }

            @Override
            public void onInfoWindowClick(Marker marker) {
                if (marker.equals(markerTEC)) {
                    TECDialogFragment.newInstance(marker.getTitle(),
                            getString(R.string.tec_full_snippet))
                            .show(getSupportFragmentManager(), null ) ;
                    }
                }


            @Override
            public boolean onMarkerClick(Marker marker) {
                if(marker.equals(markerCQ)) {
                    Intent intent = new Intent(this , MarkerDetailActivity. class ) ;
                    intent.putExtra(EXTRA_LATITUDE , marker.getPosition().latitude);
                    intent.putExtra(EXTRA_LONGITUDE , marker.getPosition().longitude);
                    startActivity(intent);
                }
                else if(marker.equals(markerTEC)) {
                    Intent intent = new Intent(this , MarkerDetailActivity. class ) ;
                    intent.putExtra(EXTRA_LATITUDE , marker.getPosition().latitude);
                    intent.putExtra(EXTRA_LONGITUDE , marker.getPosition().longitude);
                    startActivity(intent);
                }
                else if(marker.equals(markerTOSCANA)) {
                    Intent intent = new Intent(this , MarkerDetailActivity. class ) ;
                    intent.putExtra(EXTRA_LATITUDE , marker.getPosition().latitude);
                    intent.putExtra(EXTRA_LONGITUDE , marker.getPosition().longitude);
                    startActivity(intent);
                }

                return false;
            }

            @Override
            public void onMarkerDragStart(Marker marker) {
                if (marker.equals(markerCQ)) {
                    Toast.makeText(this , " START ", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onMarkerDrag(Marker marker) {
                if (marker.equals(markerCQ)) {
                    String newTitle = String.format(Locale.getDefault(),
                            getString(R.string.marker_detail_latlng),
                            marker.getPosition().latitude ,
                            marker.getPosition().longitude);
                    setTitle(newTitle);
                }
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                if (marker.equals(markerCQ)) {
                    Toast.makeText(this , " END ", Toast.LENGTH_SHORT).show();
                }
            }
}
