package com.example.juanpa.labmapas;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 *
 * to handle interaction events.
 * Use the {@link TECDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TECDialogFragment extends DialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    public static final String ARGUMENTO_TITLE = " TITLE ";
    public static final String ARGUMENTO_FULL_SNIPPET = " FULL_SNIPPET ";

    // TODO: Rename and change types of parameters
    private String title;
    private String fullSnippet;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(fullSnippet)
                .create();
        return dialog;
    }

    public TECDialogFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TECDialogFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TECDialogFragment newInstance(String param1, String param2) {
        TECDialogFragment fragment = new TECDialogFragment();
        Bundle args = new Bundle();
        args.putString(ARGUMENTO_TITLE, param1);
        args.putString(ARGUMENTO_FULL_SNIPPET, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            title = getArguments().getString(ARGUMENTO_TITLE);
            fullSnippet = getArguments().getString(ARGUMENTO_FULL_SNIPPET);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tecdialog, container, false);
    }


}
