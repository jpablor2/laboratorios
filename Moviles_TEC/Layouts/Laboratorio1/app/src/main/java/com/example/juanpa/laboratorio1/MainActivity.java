package com.example.juanpa.laboratorio1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.math.*;

public class MainActivity extends AppCompatActivity {

    static double op1=0;
    static double op2=0;
    static int operando=0;//0:suma,1:resta,2:multi,3:division
    static int turno=0;//0:primer op, 1: operando, 2: segundo op
    static int max_num_op1=0;
    static int max_num_op2=0;


    Button btn_uno,btn_dos,btn_tres,btn_cuatro,btn_cinco,btn_seis,btn_siete,btn_ocho,
            btn_nueve,btn_cero,btn_punto,btn_igual,btn_multiplicacion,btn_division,btn_resta,btn_suma;

    EditText display;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_cero = (Button)findViewById(R.id.btn_0);
        btn_uno=(Button) findViewById(R.id.btn_1);
        btn_dos = (Button)findViewById(R.id.btn_2);
        btn_tres= (Button)findViewById(R.id.btn_3);
        btn_cuatro = (Button)findViewById(R.id.btn_4);
        btn_cinco = (Button)findViewById(R.id.btn_5);
        btn_seis = (Button)findViewById(R.id.btn_6);
        btn_siete = (Button)findViewById(R.id.btn_7);
        btn_ocho = (Button)findViewById(R.id.btn_8);
        btn_nueve = (Button)findViewById(R.id.btn_9);
        btn_punto = (Button)findViewById(R.id.btn_punto);
        btn_igual = (Button)findViewById(R.id.btn_igual);
        btn_suma = (Button)findViewById(R.id.btn_suma);
        btn_resta = (Button)findViewById(R.id.btn_resta);
        btn_multiplicacion = (Button)findViewById(R.id.btn_multiplicacion);
        btn_division = (Button)findViewById(R.id.btn_division);
        display=(EditText)findViewById(R.id.txt_resultado);

        btn_uno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (turno)
                {
                    case 0:

                        if(max_num_op1<10) {
                            op1 =op1+1*(Math.pow(10,max_num_op1));
                            max_num_op1++;
                            display.setText(String.valueOf(String.valueOf(op1)));
                        }
                        break;

                    case 2:
                        if(max_num_op2<10) {
                            op2 =op2+1*(Math.pow(10,max_num_op2));
                            max_num_op2++;
                            display.setText(String.valueOf(String.valueOf(op2)));

                        }
                        break;

                }
            }
        });
        btn_dos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (turno)
                {
                    case 0:

                        if(max_num_op1<10) {
                            op1 =op1+2*(Math.pow(10,max_num_op1));
                            max_num_op1++;
                            display.setText(String.valueOf(String.valueOf(op1)));

                        }
                        break;

                    case 2:
                        if(max_num_op2<10) {
                            op2 =op2+2*(Math.pow(10,max_num_op2));
                            max_num_op2++;
                            display.setText(String.valueOf(String.valueOf(op2)));

                        }
                        break;

                }

            }
        });
        btn_tres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (turno)
                {
                    case 0:

                        if(max_num_op1<10) {
                            op1 =op1+3*(Math.pow(10,max_num_op1));
                            max_num_op1++;
                            display.setText(String.valueOf(String.valueOf(op1)));

                        }
                        break;

                    case 2:
                        if(max_num_op2<10) {
                            op2 =op2+3*(Math.pow(10,max_num_op2));
                            max_num_op2++;
                            display.setText(String.valueOf(String.valueOf(op2)));

                        }
                        break;

                }

            }
        });
        btn_cuatro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (turno)
                {
                    case 0:

                        if(max_num_op1<10) {
                            op1 =op1+4*(Math.pow(10,max_num_op1));
                            max_num_op1++;
                            display.setText(String.valueOf(String.valueOf(op1)));

                        }
                        break;

                    case 2:
                        if(max_num_op2<10) {
                            op2 =op2+4*(Math.pow(10,max_num_op2));
                            max_num_op2++;
                            display.setText(String.valueOf(String.valueOf(op2)));
                        }
                        break;

                }
                //insertarEstudiante();
                //insertarFuncionario();

            }
        });
        btn_cinco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (turno)
                {
                    case 0:

                        if(max_num_op1<10) {
                            op1 =op1+5*(Math.pow(10,max_num_op1));
                            max_num_op1++;
                            display.setText(String.valueOf(String.valueOf(op1)));
                        }
                        break;

                    case 2:
                        if(max_num_op2<10) {
                            op2 =op2+5*(Math.pow(10,max_num_op2));
                            max_num_op2++;
                            display.setText(String.valueOf(String.valueOf(op2)));
                        }
                        break;

                }
                //insertarEstudiante();
                //insertarFuncionario();

            }
        });
        btn_seis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (turno)
                {
                    case 0:

                        if(max_num_op1<10) {
                            op1 =op1+6*(Math.pow(10,max_num_op1));
                            max_num_op1++;
                            display.setText(String.valueOf(String.valueOf(op1)));
                        }
                        break;

                    case 2:
                        if(max_num_op2<10) {
                            op2 =op2+6*(Math.pow(10,max_num_op2));
                            max_num_op2++;
                            display.setText(String.valueOf(String.valueOf(op2)));
                        }
                        break;

                }
                //insertarEstudiante();
                //insertarFuncionario();

            }
        });
        btn_siete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (turno)
                {
                    case 0:

                        if(max_num_op1<10) {
                            op1 =op1+7*(Math.pow(10,max_num_op1));
                            max_num_op1++;
                            display.setText(String.valueOf(String.valueOf(op1)));
                        }
                        break;

                    case 2:
                        if(max_num_op2<10) {
                            op2 =op2+7*(Math.pow(10,max_num_op2));
                            max_num_op2++;
                            display.setText(String.valueOf(String.valueOf(op2)));
                        }
                        break;

                }
                //insertarEstudiante();
                //insertarFuncionario();

            }
        });
        btn_ocho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //insertarEstudiante();
                //insertarFuncionario();
                switch (turno)
                {
                    case 0:

                        if(max_num_op1<10) {
                            op1 =op1+8*(Math.pow(10,max_num_op1));
                            max_num_op1++;
                            display.setText(String.valueOf(String.valueOf(op1)));
                        }
                        break;

                    case 2:
                        if(max_num_op2<10) {
                            op2 =op2+8*(Math.pow(10,max_num_op2));
                            max_num_op2++;
                            display.setText(String.valueOf(String.valueOf(op2)));
                        }
                        break;

                }

            }
        });
        btn_nueve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //insertarEstudiante();
                //insertarFuncionario();
                switch (turno)
                {
                    case 0:

                        if(max_num_op1<10) {
                            op1 =op1+9*(Math.pow(10,max_num_op1));
                            max_num_op1++;
                            display.setText(String.valueOf(String.valueOf(op1)));
                        }
                        break;

                    case 2:
                        if(max_num_op2<10) {
                            op2 =op2+9*(Math.pow(10,max_num_op2));
                            max_num_op2++;
                            display.setText(String.valueOf(String.valueOf(op2)));
                        }
                        break;

                }

            }
        });
        btn_cero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //insertarEstudiante();
                //insertarFuncionario();
                switch (turno)
                {
                    case 0:

                        if(max_num_op1<10) {
                            op1 =op1+0*(Math.pow(10,max_num_op1));
                            max_num_op1++;
                            display.setText(String.valueOf(String.valueOf(op1)));
                        }
                        break;

                    case 2:
                        if(max_num_op2<10) {
                            op2 =op2+0*(Math.pow(10,max_num_op2));
                            max_num_op2++;
                            display.setText(String.valueOf(String.valueOf(op2)));
                        }
                        break;

                }

            }
        });
        btn_punto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //insertarEstudiante();
                //insertarFuncionario();

            }
        });

        btn_multiplicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(turno==1)
                {
                    operando =2;
                    turno++;

                }
                //insertarEstudiante();
                //insertarFuncionario();

            }
        });
        btn_suma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(turno==1)
                {
                    operando =0;
                    turno++;

                }

            }
        });
        btn_resta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(turno==1)
                {
                    operando =1;
                    turno++;

                }

            }
        });
        btn_division.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(turno==1)
                {
                    operando =3;
                    turno++;

                }

            }
        });
        btn_igual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double resultado=0.0;
                if(operando==0)
                    resultado=suma(op1,op2);
                else if(operando==1)
                    resultado=resta(op1,op2);
                else if (operando==2)
                    resultado=multiplicacion(op1,op2);
                else if (operando==3)
                    resultado=division(op1,op2);

                display.setText(String.valueOf(resultado));
                limpiar();
                //insertarEstudiante();
                //insertarFuncionario();

            }
        });

    }

        public void limpiar()
        {
            op1=0;
            op2=0;
            operando=-1;//0:suma,1:resta,2:multi,3:division
            turno=-1;//0:primer op, 1: operando, 2: segundo op
            max_num_op1=0;
            max_num_op2=0;
        }

    public double suma(double op1, double op2)
    {
        return op1+op2;
    }
    public double resta(double op1, double op2)
    {
        return op1-op2;
    }
    public double multiplicacion(double op1, double op2)
    {
        return op1*op2;
    }
    public double division(double op1, double op2)
    {
        return op1/op2;
    }
   /* public void turno_op( double valor){
        switch (turno)
        {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;

        }
    }
    public void turno_operando( char valor){
        switch (turno)
        {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;

        }
    }*/

}
